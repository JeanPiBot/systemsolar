﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejemplo : MonoBehaviour
{
    public GameObject miObjeto;

    public void aparecerObjecto() 
    {
        miObjeto.SetActive(true);
    }

    public void desaparecerObjecto() 
    {
        miObjeto.SetActive(false);
    }

}
