﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject prefabEnemigo;
    private int maximo_enemigos = 10;
    int numero_enemigos_actual = 0;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("GenerarEnemigo", 30, 20);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenerarEnemigo() //Definición
    {
        if ( numero_enemigos_actual < maximo_enemigos)
        {
            Instantiate(prefabEnemigo);
            numero_enemigos_actual ++;
        }
        
    }
}
