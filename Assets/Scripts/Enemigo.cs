﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemigo : MonoBehaviour
{

    public int vida = 3;
    public int comportamiento = 1;   // 1 -> Perseguir    2 -> Patrullar

    GameObject Personaje;


    // Start is called before the first frame update
    void Start()
    {

        Personaje = GameObject.Find("Personaje");
        comportamiento = Random.Range(1, 3); //Al crearse el enemigo, puede salir con comportamiento 1, o 2, aleatoriamente


        InvokeRepeating("CambiarDireccion", 10, 30);

    }

    // Update is called once per frame
    void Update()
    {
        if(comportamiento == 1) //perseguir
        {
            transform.LookAt(Personaje.transform);
            GetComponent<Rigidbody>().velocity = transform.forward * 2;
        }
        else //patrullar
        {
            GetComponent<Rigidbody>().velocity = transform.forward;
        }
    }

    void CambiarDireccion()
    {
        transform.Rotate(0,Random.Range(0,360),0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name == "Personaje")
        { 
            Debug.Log("Nos ha golpeado");
            FindObjectOfType<Player>().RecibirGolpe();
        }
        if (collision.collider.tag == "Bala")
        {
            Destroy(collision.collider.gameObject);
            vida = vida - 1;
            if (vida == 0)
                Destroy(gameObject);
        }
    }
}
