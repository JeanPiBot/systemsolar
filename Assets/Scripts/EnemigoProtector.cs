﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemigoProtector : MonoBehaviour
{

    public GameObject item;
    public int vida;
    public int velocidad;

    // Start is called before the first frame update
    void Start()
    {
        vida = Random.Range(1, 6);
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(item.transform);
        GetComponent<Rigidbody>().velocity = transform.right * velocidad;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name == "Personaje")
        {
            Debug.Log("Nos ha golpeado");
            FindObjectOfType<Player>().RecibirGolpe();
        }
    }
}
